﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BeautifulDeath.tumblr;
using Newtonsoft.Json;

namespace BeautifulDeath
{
    class Program
    {
        private const string URL = "http://api.tumblr.com/v2/blog/beautifuldeath.com/posts/photo?api_key=aHCKFrem2L8DSvsTheJUsTld585K7XQ5AqH6Sgy2OMC5CjMqz1";
        static void Main(string[] args)
        {
            if (!Directory.Exists("Photos")) Directory.CreateDirectory("Photos");

            var task = GetPhotos();
            task.Wait();
            var photos = task.Result;
            SavePhotos(photos).Wait();
            Console.ReadKey();
        }

        private static async Task SavePhotos(Dictionary<string, string> photos)
        {
            var client = new HttpClient();
            var current = 0f;

            foreach (var pair in photos)
            {
                current++;
                var downloadStream = await client.GetStreamAsync(pair.Value);
                var saveStream = File.Create(string.Format("photos/{0}.jpg", pair.Key));

                await downloadStream.CopyToAsync(saveStream);
                Console.WriteLine("Done {0}, {1} percent!", pair.Key, (current / (float)photos.Count)*100f);
            }
        }

        private static async Task<Dictionary<string, string>> GetPhotos()
        {
            var client = new HttpClient();
            var json = await client.GetStringAsync(URL + "&limit=50&tag=HBO");
            dynamic r = JsonConvert.DeserializeObject(json);
            var response = (TumblrResponse)JsonConvert.DeserializeObject<TumblrResponse>(JsonConvert.SerializeObject(r.response));

            var photos = new Dictionary<string, string>();
            foreach (var post in response.Posts)
            {
                var regex = new Regex("S[0-9]+E[0-9]+");
                var tag = post.Tags.LastOrDefault(t => regex.Match(t).Success && !(new []{"submission","fan"}).Contains(t));
                if (tag == null || photos.ContainsKey(tag)) continue;
                
                photos.Add(tag, post.Photos.First().OriginalSize.Url);
            }
            return photos;
        }
    }
}
