﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TumblrifyBackgrounds.Annotations;

namespace TumblrifyBackgrounds
{
    public interface IImageInfo
    {
        List<PhotoSize> AltSizes { get; }
        PhotoSize OriginalSize { get; }
    }

    public class PhotoSize
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Url { get; set; }
    }
}
