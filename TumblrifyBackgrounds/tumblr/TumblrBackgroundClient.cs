﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Web.Http;
using BeautifulDeath.tumblr;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TumblrifyBackgrounds
{
    public class TumblrBackgroundClient : IBackgroundClient
    {
        /// <summary>
        /// The url for the blog
        /// </summary>
        public string BlogUrl { get; private set; }

        /// <summary>
        /// The url pointing to the API for getting photo information about the blog.
        /// This is just my best guess and almost certainly won't work
        /// </summary>
        public string APIUrl { get { return string.Format("http://api.tumblr.com/v2/blog/{0}/posts/photo", BlogUrl); } }

        private string apiKey;

        private List<Predicate<string>> tagValidators;
        private string tag;

        /// <summary>
        /// Creates a new background client
        /// </summary>
        /// <param name="blogUrl">The url for the blog. This is like beautifuldeath.com</param>
        /// <param name="apiKey">The key for the API</param>
        /// <param name="tag">The tag that posts should have</param>
        public TumblrBackgroundClient(string blogUrl, string apiKey, List<Predicate<string>> tagValidators , string tag=null)
        {
            this.BlogUrl = blogUrl;
            this.apiKey = apiKey;
            this.tagValidators = tagValidators;
            this.tag = tag;
        }

        /// <summary>
        /// Gets JSON for photo posts
        /// </summary>
        /// <param name="limit">The number of posts to retrieve</param>
        /// <param name="offset">The offset to start from</param>
        /// <returns>The url for the json</returns>
        private string GetJsonUrl(int limit, int offset)
        {
            var url = string.Format("{0}?api_key={1}&limit={2}", APIUrl, apiKey, limit > 50 ? 50 : limit);
            if (offset > 0)
                url += "&offset=" + offset;
            if (string.IsNullOrEmpty(tag))
                url += "&tag=" + tag;
            return url;
        }

        /// <summary>
        /// Gets a certain number of photos to use as backgrounds (or less, if there aren't that many)
        /// </summary>
        /// <param name="count">The number of photos to get</param>
        /// <param name="offset">The offset to start from</param>
        /// <returns>The photos</returns>
        private async Task<List<IImageInfo>> GetImages(int count, int offset)
        {
            var photos = new List<IImageInfo>();
            var client = new HttpClient();

            var url = GetJsonUrl(count, offset);
            var response = await client.GetAsync(new Uri(url));

            //Makes sure bad things didn't happen
            response.EnsureSuccessStatusCode();

            //Get the json
            var json = await response.Content.ReadAsStringAsync();

            var r = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            var tumblrResponse = (TumblrResponse)JsonConvert.DeserializeObject<TumblrResponse>(r["response"].ToString());

            //If the number of photos we got is less than the limit we can't get any more
            var gotAll = tumblrResponse.Posts.Count < 50;

            foreach (var post in tumblrResponse.Posts)
            {
                //Offset gets incremented here, so we start our next batch of images
                //furthur along, and don't get the same stuff lots
                offset++;

                //Check if this post matches our search criteria
                if (post.Photos.Count > 0 && !Matches(post)) continue;

                //Decrement the number of images we need
                count--;
                photos.Add(post.Photos.First());
            }

            //If we still haven't got all the photos we need and we can get more photos, try and get some more
            if (count != 0 && !gotAll)
                photos.AddRange(await GetImages(count, offset));

            return photos;
        }

        /// <summary>
        /// Check to see if all the validators match the post.
        /// (make sure that at least one tag matches each validator)
        /// </summary>
        /// <param name="post">The post to check</param>
        /// <returns>Whether all validators are matches</returns>
        private bool Matches(TumblrPost post)
        {
            foreach (var validates in tagValidators)
            {
                if (!post.Tags.Any(t => validates(t)))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Gets a certain number of photos to use as backgrounds (or less, if there aren't that many)
        /// </summary>
        /// <param name="count">The number of photos to get</param>
        /// <returns>The photos</returns>
        public Task<List<IImageInfo>> GetImages(int count)
        {
            return GetImages(count, 0);
        }
    }
}
