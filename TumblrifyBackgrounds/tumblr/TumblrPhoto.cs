﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TumblrifyBackgrounds;

namespace BeautifulDeath.tumblr
{
    public class TumblrPhoto : IImageInfo
    {
        public string Caption { get; set; }

        [JsonProperty("alt_sizes")]
        public List<PhotoSize> AltSizes { get; set; }
        [JsonProperty("original_size")]
        public PhotoSize OriginalSize { get; set; }
    }
}
