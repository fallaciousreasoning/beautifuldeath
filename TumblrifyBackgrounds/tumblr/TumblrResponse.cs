﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeautifulDeath.tumblr
{
    public class TumblrResponse
    {
        public TumblrBlog Blog { get; set; }
        public List<TumblrPost> Posts { get; set; }
    }
}
