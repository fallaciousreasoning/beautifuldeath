﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TumblrifyBackgrounds
{
    public interface IBackgroundClient
    {
        Task<List<IImageInfo>> GetImages(int count);
    }
}
