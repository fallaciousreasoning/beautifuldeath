﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace IllBeBackgrounds
{
    public static class Settings
    {
        public static int Opens
        {
            get { return LoadSetting<int>(); }
            set { SaveSetting(value);}
        }

        public static string Blog
        {
            get { return LoadSetting<string>(); }
            set { SaveSetting(value);}
        }

        public static string Tag
        {
            get { return LoadSetting<string>(); }
            set { SaveSetting(value);}
        }

        private static void SaveSetting(object value, [CallerMemberName] string name = null)
        {
            var storage = ApplicationData.Current.LocalSettings;

            if (storage.Values.ContainsKey(name))
                storage.Values[name] = value;
            else storage.Values.Add(name, value);
        }

        private static T LoadSetting<T>([CallerMemberName] string name = null)
        {
            var storage = ApplicationData.Current.LocalSettings;
            if (!storage.Values.ContainsKey(name)) return default(T);

            return (T) storage.Values[name];
        }
    }
}
