﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using IllBeBackgrounds.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace IllBeBackgrounds.Pages
{
    public partial class BlogSettings : PhoneApplicationPage
    {
        public BlogSettings()
        {
            InitializeComponent();
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            (DataContext as TumblrSetupViewModel).AcceptCommand.Execute(null);
        }
    }
}