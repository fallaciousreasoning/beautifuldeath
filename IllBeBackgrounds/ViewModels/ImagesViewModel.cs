﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Web.Http;
using TumblrifyBackgrounds;

namespace IllBeBackgrounds.ViewModels
{
    public class ImagesViewModel
    {
        public ObservableCollection<string> ImagePaths { get; private set; }
        private List<IImageInfo> images; 

        public ImagesViewModel()
        {
            ImagePaths = new ObservableCollection<string>();
            images = TumblrSetupViewModel.Images;
            if (DesignerProperties.IsInDesignTool) return;
            DownloadImages();
        }

        public async void DownloadImages()
        {
            var storage = ApplicationData.Current.LocalFolder;
            foreach (var imageInfo in images)
            {
                await SaveImage(imageInfo.OriginalSize.Url, storage);
                ImagePaths.Add(imageInfo.OriginalSize.Url);
            }
        }

        private async Task SaveImage(string url, IStorageFolder folder)
        {
            var fileName = Guid.NewGuid().ToString() + url.Substring(url.Length-4);
            var file = await folder.CreateFileAsync(fileName);

            var writeStream = await file.OpenStreamForWriteAsync();

            var client = new HttpClient();
            var response = await client.GetAsync(new Uri(url));
            
            response.EnsureSuccessStatusCode();

            var readStream = (await response.Content.ReadAsInputStreamAsync()).AsStreamForRead();
            await readStream.CopyToAsync(writeStream);

            readStream.Close();
            writeStream.Close();

            ImagePaths.Add("ms-appdata:///local/"+fileName);
        }
    }
}
