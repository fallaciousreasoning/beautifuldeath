﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using IllBeBackgrounds.Commands;
using TumblrifyBackgrounds;
using TumblrifyBackgrounds.Annotations;

namespace IllBeBackgrounds.ViewModels
{
    public class TumblrSetupViewModel : INotifyPropertyChanged
    {
        public static List<IImageInfo> Images = new List<IImageInfo>();

        private string _tag;
        private string _regex;
        private string _blog;
        private int _imageCount;

        public string Tag
        {
            get { return _tag; }
            set
            {
                if (value == _tag) return;
                _tag = value;
                OnPropertyChanged();
            }
        }

        public string Regex
        {
            get { return _regex; }
            set
            {
                if (value == _regex) return;
                _regex = value;
                OnPropertyChanged();
            }
        }

        public string Blog
        {
            get { return _blog; }
            set
            {
                if (value == _blog) return;
                _blog = value;
                OnPropertyChanged();
            }
        }

        public int ImageCount
        {
            get { return _imageCount; }
            set
            {
                if (value == _imageCount) return;
                _imageCount = value;
                OnPropertyChanged();
            }
        }

        public RoutedCommand AcceptCommand { get; private set; }

        public TumblrSetupViewModel()
        {
            Blog = "beautifuldeath.com";
            Regex = "S[0-9]+E[0-9]+";
            Tag = "HBO";
            ImageCount = 50;

            AcceptCommand = new RoutedCommand(Test);
        }

        private async void Test()
        {
            var client = BuildClient();
            try
            {
                var images = await client.GetImages(ImageCount);
                Images.Clear();
                Images.AddRange(images);
                MessageBox.Show("got" + images.Count + " images!", "success", MessageBoxButton.OK);

                (Application.Current.RootVisual as Frame).Navigate(new Uri("/Pages/ViewImagesPage.xaml", UriKind.Relative));
            }
            catch (Exception e)
            {
                MessageBox.Show("Something went wrong!\n\n" + e.Message, "Uh oh!", MessageBoxButton.OK);
            }
        }

        public TumblrBackgroundClient BuildClient()
        {
            return new TumblrBackgroundClient(Blog, Constants.API_KEY, new List<Predicate<string>>()
            {
                (tag) => new Regex(Regex).IsMatch(tag)
            }, Tag);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
