﻿using System;
using System.Windows.Input;

namespace IllBeBackgrounds.Commands
{
    public class RoutedCommand : ICommand
    {
        private readonly Action _commandAction;

        public RoutedCommand(Action a)
        {
            _commandAction = a;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _commandAction();
        }

        public event EventHandler CanExecuteChanged;
    }
}