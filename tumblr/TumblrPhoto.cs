﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BeautifulDeath.tumblr
{
    public class TumblrPhoto
    {
        public string Caption { get; set; }

        [JsonProperty("alt_sizes")]
        public List<TumblrPhotoSize> AltSizes { get; set; }
        [JsonProperty("original_size")]
        public TumblrPhotoSize OriginalSize { get; set; }
    }

    public class TumblrPhotoSize
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Url { get; set; }
    }
}
