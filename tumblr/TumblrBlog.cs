﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BeautifulDeath.tumblr
{
    public class TumblrBlog
    {
        public string Title { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Updated { get; set; }
        public string Description { get; set; }

        [JsonProperty("is_nsfw")]
        public bool NSFW { get; set; }

        public bool Ask { get; set; }
    }
}
