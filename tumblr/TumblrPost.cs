﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BeautifulDeath.tumblr
{
    public class TumblrPost
    {
        [JsonProperty("blog_name")]
        public string BlogName { get; set; }
        public string Id { get; set; }
        public string PostUrl { get; set; }
        public string ShortUrl { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public List<string> Tags { get; set; }
        public List<TumblrPhoto> Photos { get; set; }
    }
}
